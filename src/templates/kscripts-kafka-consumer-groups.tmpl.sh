#!/usr/bin/env -S bash -euo pipefail

source ${KSCRIPTS_ROOT}/src/helpers/filename.sh

wrapper=$(helper_filename_wrapper ${cluster} ${service_account} 'consumer-groups')

cat > ${wrapper} << TEMPLATE
#!/usr/bin/env -S bash -euo pipefail
debug=\${DEBUG:-"false"}
[[ \${debug} != "false" ]] && set -x
exec kafka-consumer-groups \\
  --bootstrap-server ${bootstrap_server} \\
  --command-config ${properties} \$@
TEMPLATE

echo ${wrapper}
unset wrapper
