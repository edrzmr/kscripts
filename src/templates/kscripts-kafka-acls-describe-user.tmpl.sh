#!/usr/bin/env -S bash -euo pipefail

source ${KSCRIPTS_ROOT}/src/helpers/filename.sh

wrapper=$(helper_filename_wrapper ${cluster} ${service_account} 'acls-describe-user')

cat > ${wrapper} << TEMPLATE
#!/usr/bin/env -S bash -euo pipefail
debug=\${DEBUG:-"false"}
[[ \${debug} != "false" ]] && set -x
exec kafka-acls \\
  --bootstrap-server ${bootstrap_server} \\
  --command-config ${properties} \\
  --principal User:\${1} \\
  --list | grep -A1 'resourceType=' | awk '{ if (NR % 3) printf("%s ", \$0); else printf("%s\n", \$0) }' | tr '=' ' ' | tr -d '),' | awk '{ printf("{\"resource\":\"%s\",\"name\":\"%s\",\"op\":\"%s\",\"permission\":\"%s\"}\n", \$6, \$8, \$16, \$18) }' | sort
TEMPLATE

echo ${wrapper}
unset wrapper
