#!/usr/bin/env -S bash -euo pipefail

source ${KSCRIPTS_ROOT}/src/helpers/filename.sh

wrapper=$(helper_filename_wrapper ${cluster} ${service_account} 'acls')

cat > ${wrapper} << TEMPLATE
#!/usr/bin/env -S bash -euo pipefail
debug=\${DEBUG:-"false"}
[[ \${debug} != "false" ]] && set -x
exec kafka-acls \\
  --bootstrap-server ${bootstrap_server} \\
  --command-config ${properties} \$@
TEMPLATE

echo ${wrapper}
unset wrapper
