#!/usr/bin/env bash -euo pipefail

function helper_filename_wrapper() {
    local cluster=$1
    local service_account=$2
    local wrapper=$3
    echo -n ${KSCRIPTS_ROOT}/bin/kafka-${wrapper}-${cluster}-${service_account}.sh
}

function helper_filename_properties() {
    local cluster=$1
    local service_account=$2
    echo -n ${KSCRIPTS_PROPERTIES}/${cluster}-${service_account}.properties
}
