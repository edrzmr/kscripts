#!/usr/bin/env -S bash -euo pipefail

myself=${@:-$(basename ${0})}

function __kscript_generate_usage() {
cat << USAGE

Usage:

  ${myself} [help|-h|--help]
  ${myself} --cluster=<String> --service-account=<String> --bootstrap-server=<String> --security=NONE
  ${myself} \\
        --cluster=<String> \\
        --service-account=<String> \\
        --bootstrap-server=<String> \\
        --security=[PLAIN|SCRAM-SHA-256] \\
        --broker-username=<String> \\
        --broker-password=<String>

Parameters:

  --cluster=<String>            Cluster alias. ie: local, prod, staging, dev
  --service-account=<String>    Account alias. ie: local, eder.maria, event-collector
  --bootstrap-server=<String>   The server(s) to connect to. ie: localhost:9092
  --security=<String>           Pick right one for your cluster: (NONE|PLAIN|SCRAM-SHA-256)
  --broker-username=<String>    Broker username, unnecessary when --security=NONE
  --broker-password=<String>    Broker password, unnecessary when --security==NONE

Example:

  ${myself} \\
        --cluster=local \\
        --service-account=local \\
        --bootstrap-server=localhost:9092 \\
        --security=NONE

USAGE
}

function kscripts_command_handler() {

	bootstrap_server=""
	broker_username=""
	broker_password=""
	cluster=""
	service_account=""
	security=""

	######################
	# parse command line #
	######################
	local unknown=()
	local usage=false
	[[ ${#} -eq 0 ]] && usage=true
	for param in ${@}; do
		case ${param} in
		--bootstrap-server=*) bootstrap_server=${param#*=} ;;
		--broker-username=*)  broker_username=${param#*=} ;;
		--broker-password=*)  broker_password=${param#*=} ;;
		--security=*)         security=${param#*=} ;;
        --service-account=*)  service_account=${param#*=} ;;
		--cluster=*)          cluster=${param#*=} ;;
		help | -h | --help)   usage=true ;;
        *)           		  unknown+=${param} ;;
		esac
		shift
	done

	###################
	# validate inputs #
	###################
	[[ ${#unknown} -gt 1 ]] && echo "unknown parameters: ${unknown[@]}" && return 1
	[[ ${#unknown} -gt 0 ]] && echo "unknown parameter: ${unknown[@]}" && return 1
	[[ ${usage} == true ]] && __kscript_generate_usage && return 0


	local errors=0
	[[ ${cluster} == "" ]] && errors=$((errors+1)) && echo '--cluster=<String> must be provided'
	[[ ${service_account} == "" ]] && errors=$((errors+1)) && echo '--service-account=<String> must be provided'
	[[ ${bootstrap_server} == "" ]] && errors=$((errors+1)) && echo '--bootstrap-server=<String> must be provided'
	[[ ${security} == "" ]] && errors=$((errors+1)) && echo '--security=<String> must be provided'
	case ${security} in
    PLAIN | SCRAM-SHA-256)
    	[[ ${broker_username} == "" ]] && errors=$((errors+1)) && echo '--broker-username=<String> must be provided when --security is provided'
        [[ ${broker_password} == "" ]] && errors=$((errors+1)) && echo '--broker-password=<String> must be provided when --security is provided'
        ;;
    NONE)
        ;;
	*)
		errors=$((errors+1)) && echo '--security must be one of NONE, SCRAM-SHA-256, or PLAIN'
	esac
	[[ ${errors} -ne 0 ]] && echo "\nplease, fix above errors and try again, ${errors} error(s) found" && __kscript_generate_usage && return 1


	####################
	# render templates #
	####################
	local properties=($(source ${KSCRIPTS_ROOT}/src/templates/kscripts-properties.tmpl.sh))
	local wrappers=(
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-acls-describe-user.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-acls-list-principals.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-acls.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-console-consumer.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-console-producer.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-consumer-groups.tmpl.sh)
		$(source ${KSCRIPTS_ROOT}/src/templates/kscripts-kafka-topics.tmpl.sh)
	)
	chmod +x ${wrappers[@]}

	###################
	# display results #
	###################
	cat <<- EOF | jq
	{
		"input": {
			"--bootstrap-server": "${bootstrap_server}",
			"--broker-username": "${broker_username}",
			"--broker-password": "${broker_password}",
			"--security": "${security}",
			"--cluster": "${cluster}",
			"--service-account": "${service_account}"
		},
		"output": {
			"properties": $(printf '%s\n' ${properties[@]} | jq -R | jq -s),
			"wrappers": $(printf '%s\n' ${wrappers[@]} | jq -R | jq -s)
		}
	}
	EOF

	unset bootstrap_server \
		broker_username \
		broker_password \
		cluster \
		service_account \
		security

	return 0
}
