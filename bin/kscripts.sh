#!/usr/bin/env -S bash -euo pipefail

KSCRIPTS_ROOT=${KSCRIPTS_ROOT:-${HOME}/.kscripts}
KSCRIPTS_PROPERTIES=${KSCRIPTS_PROPERTIES:-/keybase/private/$(whoami)/kscripts/properties}

kscripts_commands=(
	$(for file in $(ls -1 ${KSCRIPTS_ROOT}/src/commands/kscripts-*-command.sh); do
		basename ${file} | cut -d- -f2
	done))

myself='kscripts'

function __kscripts_usage() {
cat << USAGE

Usage:

  ${myself} [help|-h|--help]
  ${myself} <command> help

Commands:

  ${kscripts_commands[@]}

Example:

  ${myself} generate help

USAGE
}

function kscripts() {
	local arg=${1:-none}
	[[ ${arg} != none ]] && shift

	case ${arg} in
        gen | generate)     cmd=generate ;;
		help | -h | --help) cmd=usage ;;
		none)               cmd=usage ;;
		*)                  cmd=usage ;;
    esac

	[[ ${cmd} == usage ]] && __kscripts_usage && return 0
	
	command_found=$(for i in ${kscripts_commands[@]}; do
						[[ ${i} == ${cmd} ]] && echo true && exit 0
					done
					echo false)

	if [[ ${command_found} == false ]]; then
		echo '> command not found'
		__kscripts_usage
		return 1
	fi


	source ${KSCRIPTS_ROOT}/src/commands/kscripts-${cmd}-command.sh ${0} ${arg}
	kscripts_command_handler ${@}
	return ${?}
}


export PATH="${KSCRIPTS_ROOT}/bin:${PATH}"

echo
echo 'loaded \o/'
echo
